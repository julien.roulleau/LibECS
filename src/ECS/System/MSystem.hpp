#ifndef MSYSTEM_HPP
#define MSYSTEM_HPP

#include <vector>
#include "ISystem.hpp"

namespace ECS {
	class MSystem {
	public:
		static MSystem &getInstance();

		MSystem(const MSystem &) = delete;
		MSystem &operator=(const MSystem &) = delete;

		static void add(SourceSystem *system);
		static void updateAll(DeltaTime delta);

	private:
		MSystem() = default;
		~MSystem() = default;

		std::vector<SourceSystem *> systems;
	};
}

#endif /* MSYSTEM_HPP */
