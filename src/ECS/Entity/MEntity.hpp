#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <map>
#include <vector>
#include <ECS/Component/IMComponent.hpp>

namespace ECS {
	typedef unsigned long Entity;

	class MEntity {
	public:
		static MEntity &getInstance();

		MEntity(const MEntity &) = delete;
		MEntity &operator=(const MEntity &) = delete;

		static Entity create();
		static void remove(Entity entity);

		static void addComponent(Entity entity, IMComponent *component);
		static void removeComponent(Entity entity , IMComponent *component);

	private:
		MEntity() = default;
		~MEntity() = default;

		std::map<Entity, std::vector<IMComponent *>> entities;

		unsigned long last{0};
	};
}

#endif /* ENTITY_HPP */
