#ifndef IMEVENT_HPP
#define IMEVENT_HPP

namespace ECS {
	class IMEvent {
	public:
		IMEvent() = default;
		virtual ~IMEvent() = default;

		virtual void clear() = 0;

	private:
	};
}


#endif /* IMEVENT_HPP */
