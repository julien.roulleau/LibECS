#ifndef EVENTSTORE_HPP
#define EVENTSTORE_HPP

#include <vector>
#include "IMEvent.hpp"

namespace ECS {
	class EventStore {
	public:
		static EventStore &getInstance() {
			static EventStore instance;
			return instance;
		}

		EventStore(const EventStore &) = delete;
		EventStore &operator=(const EventStore &) = delete;

		static void store(IMEvent *event);
		static void clearAll();

	private:
		EventStore() = default;
		~EventStore() = default;

		std::vector<IMEvent *> pool;
	};
}

#endif /* EVENTSTORE_HPP */
