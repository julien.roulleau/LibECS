#ifndef IMCOMPONENT_HPP
#define IMCOMPONENT_HPP

namespace ECS {

	typedef unsigned long Entity;

	class IMComponent {
	public:
		virtual ~IMComponent() = default;
		IMComponent() = default;

		virtual void erase(Entity entity) = 0;

	private:
	};
}

#endif /* IMCOMPONENT_HPP */
