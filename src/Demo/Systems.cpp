#include <ECS/Component/MComponent.hpp>
#include <ECS/Event/MEvent.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Window/Event.hpp>

#include "Systems.hpp"
#include "Components.hpp"
#include "Demo.hpp"
#include "Entitys.hpp"
#include "Events.hpp"

void SPhysic::update(ECS::DeltaTime delta) {
	for (auto &component : ECS::MComponent<CSpeed>::getAll()) {
		auto &speed = component.second;
		auto pos = ECS::MComponent<CPos>::get(component.first);

		pos->vec.x += speed.vec.x * delta;
		pos->vec.y += speed.vec.y * delta;

		if (pos->vec.y < RADIUS) {
			pos->vec.y = RADIUS;
			speed.vec.y = -speed.vec.y;
		}

		if (pos->vec.y > HEIGHT - RADIUS) {
			pos->vec.y = HEIGHT - RADIUS;
			speed.vec.y = -speed.vec.y;
		}

		if (pos->vec.x < RADIUS) {
			pos->vec.x = RADIUS;
			speed.vec.x = -speed.vec.x;
		}

		if (pos->vec.x > WIDTH - RADIUS) {
			pos->vec.x = WIDTH - RADIUS;
			speed.vec.x = -speed.vec.x;
		}
	}
}

void SRender::preUpdate(ECS::DeltaTime delta) {
	(void) delta;
	ECS::MComponent<CWindow>::getAll().begin()->second.window->clear(
		sf::Color::White);
}

void SRender::update(ECS::DeltaTime delta) {
	(void) delta;
	auto &window = ECS::MComponent<CWindow>::getAll().begin()->second.window;
	for (auto &component : ECS::MComponent<CColor>::getAll()) {
		auto color = component.second;
		auto pos = ECS::MComponent<CPos>::get(component.first);

		sf::CircleShape shape(RADIUS);
		shape.setFillColor(color.color);
		shape.setOrigin(RADIUS, RADIUS);

		shape.setPosition(pos->vec);

		window->draw(shape);
	}
}

void SRender::postUpdate(ECS::DeltaTime delta) {
	(void) delta;
	ECS::MComponent<CWindow>::getAll().begin()->second.window->display();
}

void SRender::handleEvents() {
	void *event;
	while (ECS::MEvent<EClose, void *>::pop(event)) {
		ECS::MComponent<CWindow>::getAll().begin()->second.window->close();
	}
}

void SInput::update(ECS::DeltaTime delta) {
	(void) delta;
	sf::Event event{};
	auto &window = ECS::MComponent<CWindow>::getAll().begin()->second.window;
	while (window->pollEvent(event)) {
		switch (event.type) {
			case sf::Event::Closed:
				ECS::MEvent<EClose, void *>::push(
					nullptr);
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
					ECS::MEvent<EClose, void *>::push(
						nullptr);
				break;
			case sf::Event::MouseButtonPressed: {
				switch (event.mouseButton.button) {
					case sf::Mouse::Left:
						ECS::MEvent<EMouseLeft, sf::Vector2f>::push(
							{
								(float) event.mouseButton.x,
								(float) event.mouseButton.y
							});
						break;
					case sf::Mouse::Right:
						ECS::MEvent<EMouseRight, void *>::push(
							nullptr);
						break;
					default:
						break;
				}
			}
			default:
				break;
		}
	}
}

void SBall::handleEvents() {
	sf::Vector2f mouse;
	while (ECS::MEvent<EMouseLeft, sf::Vector2f>::pop(mouse)) {
		for (int i = 0; i < 100; i++)
			createBall({mouse.x, mouse.y});
	}

	void *rm;
	while (ECS::MEvent<EMouseRight, void *>::pop(rm)) {
		auto balls = ECS::MComponent<CColor>::getAll();
		if (balls.begin() != balls.end())
			ECS::MEntity::remove(balls.begin()->first);
	}
}
