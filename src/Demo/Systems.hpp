#ifndef SYSTEMS_HPP
#define SYSTEMS_HPP

#include <ECS/System/ISystem.hpp>

class SPhysic : public ECS::ISystem<SPhysic> {
public:
	void update(ECS::DeltaTime delta) override;
};

class SRender : public ECS::ISystem<SRender> {
public:
	void preUpdate(ECS::DeltaTime delta) override;
	void update(ECS::DeltaTime delta) override;
	void postUpdate(ECS::DeltaTime delta) override;

	void handleEvents() override;
};

class SInput : public ECS::ISystem<SInput> {
public:
	void update(ECS::DeltaTime delta) override;
};

class SBall : public ECS::ISystem<SBall> {
	void handleEvents() override;
};

#endif /* SYSTEMS_HPP */
