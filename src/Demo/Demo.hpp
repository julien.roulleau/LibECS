#ifndef DEMO_HPP
#define DEMO_HPP

#define WIDTH  800
#define HEIGHT 600

#define RADIUS 20

int demo();

#endif /* DEMO_HPP */
