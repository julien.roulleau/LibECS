#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

class CWindow {
public:
	CWindow() = default;
	explicit CWindow(sf::RenderWindow *w) : window(w){}
	sf::RenderWindow *window;
};


class CPos {
public:
	CPos() = default;
	explicit CPos(sf::Vector2f v) : vec(v){}
	sf::Vector2f vec;
};

class CSpeed {
public:
	CSpeed() = default;
	explicit CSpeed(sf::Vector2f v) : vec(v){}
	sf::Vector2f vec;
};

class CColor {
public:
	CColor() = default;
	explicit CColor(sf::Color c) : color(c){}
	sf::Color color;
};

#endif /* COMPONENTS_HPP */
