#include <ECS/Component/MComponent.hpp>
#include <ECS/System/MSystem.hpp>
#include <ECS/Event/MEvent.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>

#include "Demo.hpp"
#include "Components.hpp"
#include "Entitys.hpp"
#include "Systems.hpp"

int demo() {
	std::srand(static_cast<unsigned int>(std::time(nullptr)));
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "demo",
				sf::Style::Titlebar | sf::Style::Close);
	ECS::MComponent<CWindow>::create(ECS::MEntity::create(), CWindow{&window});

	ECS::MSystem::add(&SPhysic::getInstance());
	ECS::MSystem::add(&SRender::getInstance());
	ECS::MSystem::add(&SInput::getInstance());
	ECS::MSystem::add(&SBall::getInstance());
	SPhysic::start();
	SRender::start();
	SInput::start();
	SBall::start();

	sf::Clock clock;
	ECS::DeltaTime timeDelta = 0;

	while (window.isOpen()) {
		sf::Time elapsed = clock.restart();
		ECS::MSystem::updateAll(timeDelta);
		timeDelta = elapsed.asSeconds();
	}

	return 0;
}
