#include <ECS/Entity/MEntity.hpp>
#include <ECS/Component/MComponent.hpp>
#include "Entitys.hpp"
#include "Components.hpp"

ECS::Entity createBall(sf::Vector2f pos) {
	ECS::Entity entity = ECS::MEntity::create();

	ECS::MComponent<CPos>::create(entity, CPos{pos});

	ECS::MComponent<CSpeed>::create(entity, CSpeed{
		{
			static_cast<float>(std::rand() % 500),
			static_cast<float>(std::rand() % 500)
		}
	});

	ECS::MComponent<CColor>::create(entity, CColor{
		{
			static_cast<sf::Uint8>(std::rand() % 256),
			static_cast<sf::Uint8>(std::rand() % 256),
			static_cast<sf::Uint8>(std::rand() % 256),
			192
		}
	});

	return entity;
}
